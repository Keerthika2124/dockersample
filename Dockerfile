FROM openjdk:17
EXPOSE 8080
ADD target/springDocker.jar springzDocker.jar
ENTRYPOINT ["java","-jar","/springDocker.jar"]